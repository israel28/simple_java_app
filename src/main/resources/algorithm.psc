Algoritmo BANT
	presupuesto = 0
	autoridad = 0
	necesidad = 0
	tiempo = 0
	calificacion = ''
	Dimension buffer_presupuesto[1],buffer_autoridad[1],buffer_necesidad[1],buffer_tiempo[1]
	Escribir 'Para esta oportunidad �Cuenta ya con alg�n presupuesto aprobado?'
	Escribir '(A): No tiene presupuesto'
	Escribir '(B): No tiene presupuesto pero le gustar�a tenerlo'
	Escribir '(C): Tiene presupuesto pero no es suficiente'
	Escribir '(D): Tiene presupuesto y es suficiente'
	Escribir '(E): Tiene presupuesto y se excede'
	Escribir 'La opc��n indicada por el cliente es:'

	entrada_correcta_presupuesto<-Falso

	Repetir

		Leer letra_presupuesto
		buffer_presupuesto[1]<-letra_presupuesto

		Si (buffer_presupuesto[1])=('A')|(buffer_presupuesto[1])=('B')|(buffer_presupuesto[1])=('C')|(buffer_presupuesto[1])=('D')|(buffer_presupuesto[1])=('E') Entonces
			entrada_correcta_presupuesto<-Verdadero
		SiNo
			entrada_correcta_presupuesto<-Falso
			Escribir 'Introduzca una opci�n v�lida, vuelva a intentar:'
		Fin Si

	Hasta Que entrada_correcta_presupuesto=Verdadero

	Si buffer_presupuesto[1]=='A' Entonces
		presupuesto<-0
	SiNo
		Si buffer_presupuesto[1]=='B' Entonces
			presupuesto<-1
		SiNo
			Si buffer_presupuesto[1]=='C' Entonces
				presupuesto<-2
			SiNo
				Si buffer_presupuesto[1]=='D' Entonces
					presupuesto<-3
				SiNo
					Si buffer_presupuesto[1]=='E' Entonces
						presupuesto<-4
					Fin Si
				Fin Si
			Fin Si
		Fin Si
	Fin Si

	Limpiar Pantalla

	Escribir '�Es usted quien tomar� la decisi�n de concretar dicha oportunidad?'
	Escribir '(A): No toma la decisi�n ni tiene acceso a quien la toma'
	Escribir '(B): No toma la decisi�n pero tiene acceso a quien la toma'
	Escribir '(C): No toma la decisi�n pero influye en quien la toma'
	Escribir '(D): Toma la decisi�n en conjunto con otras personas'
	Escribir '(E): Es quien toma la decisi�n'
	Escribir 'La opc��n indicada por el cliente es:'

	entrada_correcta_autoridad<-Falso

	Repetir

		Leer letra_autoridad
		buffer_autoridad[1]<-letra_autoridad

		Si (buffer_autoridad[1])=('A')|(buffer_autoridad[1])=('B')|(buffer_autoridad[1])=('C')|(buffer_autoridad[1])=('D')|(buffer_autoridad[1])=('E') Entonces
			entrada_correcta_autoridad<-Verdadero
		SiNo
			entrada_correcta_autoridad<-Falso
			Escribir 'Introduzca una opci�n v�lida, vuelva a intentar:'
		Fin Si

	Hasta Que entrada_correcta_autoridad=Verdadero

	Si buffer_autoridad[1]=='A' Entonces
		autoridad<-0
	SiNo
		Si buffer_autoridad[1]=='B' Entonces
			autoridad<-1
		SiNo
			Si buffer_autoridad[1]=='C' Entonces
				autoridad<-2
			SiNo
				Si buffer_autoridad[1]=='D' Entonces
					autoridad<-3
				SiNo
					Si buffer_autoridad[1]=='E' Entonces
						autoridad<-4
					Fin Si
				Fin Si
			Fin Si
		Fin Si
	Fin Si


	Limpiar Pantalla

	Escribir '�La necesidad de implementar es imperante? '
	Escribir '(A): No tiene necesidad'
	Escribir '(B): No tiene necesidad pero le gustar�a tenerla'
	Escribir '(C): Tiene necesidad pero no es indispensable'
	Escribir '(D): Tiene necesidad, pero tiene con que cubrirla'
	Escribir '(E): Tiene necesidad y no sabe c�mo solucionarlo'
	Escribir 'La opc��n indicada por el cliente es:'


	entrada_correcta_necesidad<-Falso

	Repetir

		Leer letra_necesidad
		buffer_necesidad[1]<-letra_necesidad

		Si (buffer_necesidad[1])=('A')|(buffer_necesidad[1])=('B')|(buffer_necesidad[1])=('C')|(buffer_necesidad[1])=('D')|(buffer_necesidad[1])=('E') Entonces
			entrada_correcta_necesidad<-Verdadero
		SiNo
			entrada_correcta_necesidad<-Falso
			Escribir 'Introduzca una opci�n v�lida, vuelva a intentar:'
		Fin Si

	Hasta Que entrada_correcta_necesidad=Verdadero

	Si buffer_necesidad[1]=='A' Entonces
		necesidad<-0
	SiNo
		Si buffer_necesidad[1]=='B' Entonces
			necesidad<-1
		SiNo
			Si buffer_necesidad[1]=='C' Entonces
				necesidad<-2
			SiNo
				Si buffer_necesidad[1]=='D' Entonces
					necesidad<-3
				SiNo
					Si buffer_necesidad[1]=='E' Entonces
						necesidad<-4
					Fin Si
				Fin Si
			Fin Si
		Fin Si
	Fin Si

	Limpiar Pantalla

	Escribir '�En que tiempo debe implementarse esta oportunidad o soluci�n? '
	Escribir '(A): No tiene definido cuando'
	Escribir '(B): Despu�s de un a�o'
	Escribir '(C): Entre 6 y 12 meses'
	Escribir '(D): Entre 3 y 6 meses'
	Escribir '(E): En menos de 3 meses'
	Escribir 'La opc��n indicada por el cliente es:'




	entrada_correcta_tiempo<-Falso

	Repetir

		Leer letra_tiempo
		buffer_tiempo[1]<-letra_tiempo

		Si (buffer_tiempo[1])=('A')|(buffer_tiempo[1])=('B')|(buffer_tiempo[1])=('C')|(buffer_tiempo[1])=('D')|(buffer_tiempo[1])=('E') Entonces
			entrada_correcta_tiempo<-Verdadero
		SiNo
			entrada_correcta_tiempo<-Falso
			Escribir 'Introduzca una opci�n v�lida, vuelva a intentar:'
		Fin Si

	Hasta Que entrada_correcta_tiempo=Verdadero
	
	Si buffer_tiempo[1]=='A' Entonces
		tiempo<-0
	SiNo
		Si buffer_tiempo[1]=='B' Entonces
			tiempo<-1
		SiNo
			Si buffer_tiempo[1]=='C' Entonces
				tiempo<-2
			SiNo
				Si buffer_tiempo[1]=='D' Entonces
					tiempo<-3
				SiNo
					Si buffer_tiempo[1]=='E' Entonces
						tiempo<-4
					Fin Si
				Fin Si
			Fin Si
		Fin Si
	Fin Si
	
	
	Limpiar Pantalla
	
	suma <- presupuesto+autoridad+necesidad+tiempo
	
	Escribir 'En una escala de : 0 - 16, esta oportunidad tiene una calificaci�n de:  ' , suma
	
	Si (suma>=0) Y (suma<=4) Entonces
		calificacion <- 'Oportunidad descalificada'
	FinSi
	Si (suma>=5) Y (suma<=8) Entonces
		calificacion <- 'Oportunidad poco calificada'
	FinSi
	Si (suma>=9) Y (suma<=12) Entonces
		calificacion <- 'Oportunidad calificada'
	FinSi
	Si (suma>=13) Y (suma<=16) Entonces
		calificacion <- 'Oportunidad altamente_calificada'
	FinSi
	Escribir '...por lo tanto esta oportunidad es una:  ' , calificacion
	
	
	
FinAlgoritmo